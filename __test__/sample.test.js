describe('Test sample', () => {
  it('Test 1 + 1 = 2', () => {
    expect(1 + 1).toBe(2);
  });
  it('Test SayHi', () => {
    expect(SayHi('Eduardo')).toBe('Hola, Eduardo!');
  });
});