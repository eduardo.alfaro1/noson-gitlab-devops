const SayHi = (name) => {
  return `Hola, ${name}!`;
};

export default SayHi;
